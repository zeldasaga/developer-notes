# README #


### What is this repository for? ###

This repository will contain an ever growing list of developer notes that I find useful in my career.  
Some items will be how-to guides, some a list of gotchas or special considerations, some will be programming language recipes, and whatever else I deem worthy of documenting for later use.
