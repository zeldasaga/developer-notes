# README #

### Developing For Kubernetes Locally

##### The Kubernetes Development Cycle

The usual kubernetes development lifecycle comprises of <> steps
1. Make code changes in your editor of choice.
2. (Re)Build your docker image.
3. Push your newly (re)built image to a docker registry.  This registry could be local or remote.
4. Deploy your kubernetes application by pulling the images found in the registry.

This cycle itself is pretty straightforward, but can be very slow and/or complicated to implement for the modern developer.  For instance, after making code changes and saving a file many editors provide some mechanism for automatically re-deploying your application with the new codes changes reflected.  This means a developer can test the implemented code changes within a matter of seconds.

The kubernetes lifecycle above undoes much of that convenience.  Now developers must add the time of building a docker image, pushing the image over a network, and then deploying to a service before being able to see code changes.  All of these steps can easily add minutes to each code change.  And that waiting time is both mind-numbing for the developer and expensive for the company.  So what to do?  Here are some options, in my order of preference.

##### Using minikube with your current build processes

###### Quick Reference for Windows

	- Install Docker Desktop for Windows (if run into Docker Failed to Start delete %appdata%\Docker  
and %appdata\Docker Desktop)
	- Configure it for WSL2 and the version(s) of Linux installed
	- Follow this blog post: https://kubernetes.io/blog/2020/05/21/wsl-docker-kubernetes-on-the-windows-desktop/

###### Quick Reference

    - Install minikube
    - Add/Copy registry certificate to ~/.minkube/files/etc/docker/certs.d (Optional. For third party containers not being built)
    - Delete the current minikube environment: `minikube delete`
    - Start minikube with the --mount option: `minikube start --mount --mount-string "<local-directory>:<vm-directory>" --driver=docker`
    - In the terminal session you intend to use, set the minikube environement variable: `eval $(minikube -p minikube docker-env)`
    - Build your docker containers

The problem with minikube is that it has a docker-daemon isolated from your local system.  This means that docker images built on the local system are unknown to minikube.  It is possible to give minikube these images via a copy command (`minikube image load <image-name>`), but this adds a step to our deployment that can be time consuming for larger images.  Adding additional time to the process contradicts what we are trying to achieve, so that option is not viable.
What *is* viable is pointing our terminal session to the minikube docker-daemon using an environment variable.
    `eval $(minikube -p minikube docker-env)`
Pointing to the minikube docker-daemon allows future container builds to be placed inside minikube.  The downside to this approach is all containers have to be rebuilt in order to be available.  Additionally, any calls made to local files inside of your build process may end up failing as well if they aren't mounted into the minikube virtual machine.  To mount your project's files in the minikube VM you must start minikube with the mount option (this cannot be done on an already running minikube instance):
    `minikube start --mount --mount-string "<local-directory>:<vm-directory>" --driver=docker`
As noted, this cannot be done on a current invocation of minikube. This means if you already have a minikube instance you'll have to delete your current minikube and start another with the mount option.
    `minikube delete`
Once the new minikube is up and your containers are built inside of it, it should be possible to run `kubectl` or `helm` commands to bring up a pod, deployment, or stateful set as necessary.  Linking this behavior to your save button will eliminate the need to push images to a repository and then pull images into minikube.

*Cons* to this approach are as follows:
- `eval` command *must* be invoked in every new terminal session
- Minikube needs to have every docker container built inside it.  This can be time consuming for large projects
- If you need to delete minikube, you will need to rebuild the entire project on every restart. This can be time consuming
*Pros* to this approach are as follows:
- You can reuse many (if not all) of your existing build commands by simply mounting your project directory inside the minikube VM and building your containers directly to minikube's docker-daemon
- Relatively short set-up time without a lot of complicated third party tools or dependencies
