### Core Concept React Notes

#### React Components
##### Synax

React components can be written in two ways: as functions or as classes. Functions should be  
the go-to option, but classes provide some extra options that can be useful. Examples:


```javascript
const MyComponent = (props) => {
    return (  
        <domElementOrComponent ... />  
    );  
}
```

```javascript
class MyComponent extends React.Component {  
    render () {  
        return (  
            <domElementOrComponent ... />  
        );  
    }  
}
```  

Note: Component names *MUST* start with an upper-case letter  

##### State  

Ways to use states:  
```javascript
// Using Array Destructuring
const [stateVarOfAnyType, FunctionToUpdateState] = useState(initialStateValue);
```

#### JSX with Babel

Babel is responsible for compiling Javascript calls that are HTML-like into true Javascript  
that creates HTML (through React API function calls).  Use the Babel repl for an example: https://babeljs.io/repl  

JSX supports dynamic expressions if placed within {}.