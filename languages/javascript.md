### Javascript Variables

Use `let` in favor of `var` to prevent access leaks from block scopes.

Block scopes are `{}`, which are different than function scopes which are function `sum(a,b){}`

The `const` keyword isn't immutable. The *reference* cannot change, but the contents of the  
Javascript object can. Scalar values, such as Integers or Strings, *are* immutable. This means  
combining the `const` keyword and a scalar value will essentially make the variable immutable.  
Arrays and objects defined with the `const` keyword will always point to the same object,  
but that object content may change over the lifetime of the variable if the developer is not  
careful.

### Javascript Functions

Regular functions care about what piece of code calls it. The "this" keyword will refer to the  
piece of code that called the function (the "parent" scope), not the code inside the function  
(the "local" scope). 

```javascript
const x = function () {
	// "this" here is the caller of x
}

Arrow functions don't care about what piece of code calls it, and it works well with closures.  
The "this" keyword will refer to the scope available to the function at definition.

```javascript
const y = () => {
	// "this here is NOT the caller of y
	// It is the same "this" found in the scope of y
}

### Javascript Objects

Object literals are a shorthand way to define an object rather than using the `new` keyword.  
Functions can be put into an object using a shorthand that does not require a key to be used  
(as shown in the example). Arrow functions can be defined using a key value. Dynamic properties  
look like an array, but are not. Javascript will evaluate the dynamic property and substitue  
the value in as the property name. Additionally, the shorthand for including a key with the same  
name as a variable in the parent scope is to just use the variable name.

```javascript
const dynamic = 'dValue';
const sameName = 'samesies';
const obj = {
	p1: 10,
	p2: 20,
	f1() {},
	f2: () => {},
	[dynamic]: 42,
	sameName, // equivalent to sameName: sameName,
};
console.log(obj.dynamic); //evaluates to undefined. The real property name is obj.dValue 
```

### Destructuring

Objects can have certain keys pulled out and used like variables within a particular scope. For  
instance, when importing objects from a library we can use something like `const {PI, E, SQRT2} = Math;`  
to allow us to use the PI, E, and SQRT2 properties as variable names (instead of having to write Math.PI  
all of the time).

This destructuring can also be used within functions to pass parameters. If a parameter list includes  
an object, and we want to use only a particular property/key of that object, we can enclose the parameter  
in curly braces and use that within our function.

```javascript
const circle = {
	label: 'circleX',
	radius: 2,
};

const circleArea = ({radius}) => //radius is a property/key of object circle above.
	(PI * radius * radius).toFixed(2); //radius is able to be used like a variable
```

Destructuring also works for Arrays. Instead of using keys, the array is destructured positionally  
`const [first, second,, fourth] = [1, 2, 3, 4];`

### Rest and Spread

When destructuring an array, it can be useful to only get a few items and leave the rest of the array or  
object. This destructures the wanted items into variables, and anything after the `...` will be put into a new  
array with the reference of the provided variable name.

```javascript
const [first, ...restOfItems] = [10, 20, 30, 40];
console.log(first); //10
console.log(restOfItems); // [20, 30, 40]
```

Spread is the term for using `...`. It can be used with arrays or objects and is useful for making shallow copys  
(nested objects will still be shared by both objects) of arrays and objects. `const newArray = [...oldArray];` and  
`const newObject = { ...oldObject };`

### Strings

Single quote and double quotes are basically the same. Template strings on the other hand use the back tick  
symbol and allow making strings with dynamic values inside of them. This mechanism uses *interpolation* to  
inject variable data into the string at the location of `${}`. Back ticks also allow multi-line strings  

```javascript
const stringVar = 'it is a dynamic string';
const stringLiteral = `Look ma, ${stringVar}` //result: 'Look ma, it is a dynamic string'
```

### Classes

Modern Javascript allows object oriented programming through classes. It also allows inheritance, function  
overriding, and uses of `this` keyword in the class instances. Pretty standard stuff.  

```javascript
class Person {
	constructor(name) {
		this.name = name;
	}
	greet() {
		console.log(`Hello ${this.name}!`); //this.name is for this instance of Person class
	}
}
class Student extends Person {
	constructor(name, level) {
		super(name);
		this.level = level; //this.level is for this instance of Student
	}
	greet() {
		console.log(`Hello ${this.name} from ${this.level}`);
	}
}
const o1 = new Person("Max");
const o2 = new Student("Tina", "1st Grade");
const o3 = new Student("Mary", "2nd Grade");
o3.greet = () => console.log('I am special!');

o1.greet(); //'Hello Max!'
o2.greet(); //'Hello Tina from 1st Grade!'
o3.greet(); //'I am special!'
```

### Promises and Async/Await

Using `.then()` for promises is not the modern way to handle promises and asynchronous data. The  
modern approach is to use `async () => {}` as the function call and `await` with the callbacks.

```javascript
// Instead of the following
const fetchData = () => {
	fetch('https://api.github.com').then(resp => {
		resp.json().then(data => {
			console.log(data)
		});
	});
};

// Do this
const fetchData = async () => {
	const resp = await fetch('https://api.github.com');
	const data = await resp.json();
	console.log(data);
};
fetchData();

#### Credits

Many examples in this document are taken from or derived from Samer Buna and his Plurasight course  
React: Getting Started. Many thanks to his great educational content.
