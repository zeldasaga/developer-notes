### Create React app

Start a React project for a new single-page application:  
 `npx create-react-app my-app`  
 `cd my-app`  
 `npm start`

### Install MUI

Add material UI to the project:  
`npm install @mui/material @emotion/react @emotion/styled`

### Setup Python

Setup a virtual environment:  
`virtualenv venv`
`source venv/Scripts/activate`
