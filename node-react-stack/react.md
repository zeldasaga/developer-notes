### React Core Concepts

Writing in React will often mean that written code doesn't affect the DOM directly.  
React uses JSX to convert HTML-like code into true HTML when it is built.  

Function and class components are considered separate element types. Learn to write both,  
and don't try to use both at the same time (prefer function components when possible).

#### Syntax
`ReactDOM.render()` is shorthanded to simply `render()` if used in a class that extends  
`Component`. Components rendered by a class also do not have to specify which DOM node  
they will be attached to. That is taken care of by the placement in the js file it is  
called in.

#### Props

Props are immutable. In order to change a prop of a component, it must be re-rendered by  
the DOM.

#### State

State is not immutable, and may change at any time.
