# From Lyra Template
### Components to Know About
- Open L_LyraFrontEnd level located in Content/System/FrontEnd/Maps
- The template uses a main level and background level design pattern
- The meat of the system is located in the B_LoadRandomLobbyBackground blueprint located in Environments
- L_ShooterFrontendBackground contains the background geometry for the level. It somehow has a Primary Asset Type of LyraLobbyBackground.
- This background level contains a level sequence that is played as the background. This sequence is named SEQ_LobbyScreen and is located in Plugins/ShooterMaps Content/LevelSequence
- Data asset for example playlist and frontend hold some of the definitions needed
- Loading flow is: Starting map of L_LyraFrontEnd
- Three places a widget will be overriden: Project Settings, DA_Frontend, and W_LoadingScreen_Host (or duplicated widget) Event Graph.  The priority list seems to be: 1. Project Settings 2. W_LoadingScreen_Host Event Graph 3. DA_Frontend

## Recipe
1. Create a level and populate it with the actors you want. This will be your 'background level'.
2. Duplicate W_LoadingScreen_Host from Content/UI/Foundation/LoadingScreen.
3. Open project settings and set loading screen to your new one. The setting is under Game > Common Loading System > Display > Loading Screen Widget
4. Edit this widget directly or build a child widget. 
5. If building a child widget, go into the Event Graph and under the OnLoadingScreenWidgetChanged event, update the SET command for the default widget to be your new child class
6. Find ShooterGameLobbyBG in Plugins/ShooterMaps Content/Items/Backgrounds and duplicate it.  Set the new Data asset to point to your level created earlier. The location of this file is important to the magic happening behind the scenes.

# From Scratch Using Common UI
### Components to Know About
- Enable the Common UI plugin
- In Project Settings, change the Game Viewport Client Class to CommonGameViewportClient.  This allows the topmost viewport in the rendering hierarchy to take focus, allowing stackable menu components.
- Three main components are needed
  1. Data Assets

    a. Data Table of Common Input Action Database to hold all of the input actions that can be recognized in a UI. There can be multiple databases for specialized UI flows in your game. Right click > Miscellaneous > Common UI InputActionDataTable. See Common UI YouTube video at around the 20 minute mark.
    
    b. Controller data with CommonInputBaseControllerData. Make a blueprint class for each of your inputs (keyboard, controller, etc.) These data only blueprints contain the button icons. The button mappings are contained under Input Brush Data Map.  Why it is called input brush, idk. Set all your icons to the UI texture group via bulk matrix edit. See Common UI YouTube video at around the 26 minute 25 second mark.

      i. Prepare all of your button icon images adding them to the UI Texture Group. Select all images, bulk edit property matrix, search for 'group', under LevelOfDetail use the dropdown to select the UI texture group.

    c. CommonUIInputData blueprint class. This blueprint class allows universal click/accept action and a universal back button action. In the details panel choose the data table from part a and the corresponding row entry for each of the actions. See Common UI YouTube video at around the 36 minute and 30 second mark.

    d. Under Project Settings > Game > Common Input Settings > Platform Input and set which controller data blueprints defined in part a, b, and c, above are allowed on each platform. See Common UI YouTube video at around the 38 minute mark.

  2. Styling Assets

    a. CommonBorderSyle blueprint. Pretty simple, can set a tint, image, etc. 'Border' may be a bit of a misnomer here as it will fill the in between spaces of your components such as buttons rather than simply border them with bounds.

    b. CommonButtonStyle blueprint. Allows for a lot of configuration for sounds, text style, enabled/disabled variations, and more. Normal > Normal Base will set the button color. Normal > Normal Hovered and Normal > Normal Pressed will configure the color for those states as well. For Accessibility don't forget to modify the tone for colorblind folk.

    c. CommonTextScrollStyle blueprint 

    d. CommonTextStyle. Can remove Event Graph since it is data only.  If no Font Family options, open the Content Browser, select the gear icon, and click Show Engine Content. Checkpoint - add a CommonText object to a widget blueprint.

    e. Pro tip - Can set the border, button, and text styles globally using Project Settings under Plugins -> Common UI Editor. Override precedence: 1)Project Settings, 2)Widget blueprint Common style dropdown, 3)Style set directly in Appearance section of details panel of widget blueprint
  
  3. Make the widget using CommonActivationWidget (NOT regular widget blueprint).  Inside add an overlay object (use canvas objects as INFREQUENTLY as possible). Directly under the overlay, put a CommonActivateableWidgetStack. This is kind of like a layering system, where the next CommonActivateableWidgetStack will have a higher rendering priority and will hide the previous stack until it is removed/resolved. If you want a modal or popup type style of stack, add the next CommonActivateableWidgetStack as a sibling to the first.
    a. This is like a programming stack. Widgets are pushed onto the stack and then popped off the stack as needed.

    b. Making a button. Use CommonButtonBase or CommonBoundActionButton blueprint class to make a generic (or specific) button. 
      i. Drop an Overlay component in to get all the options and a details panel.
      ii. Set the style to use the button class in step 2.b. above.  At the top of the viewport the button dimensions can be set specifically.
      iii. Add a CommonUIText object, put it in the button, and in the details panel check the Text Block Is Variable option. Pro tip - ALWAYS use Text variable type to enable easy localization. In the Event Graph pull off Pre-Construct (this allows to view it in editor) and Set the Text Block using a Get object reference. Also pull in a Get reference for the Text variable and plug it into this node.

    c. In the main menu widget (of type CommonActivateionWidget), drop an overlay and a vertical box to contain your buttons. Drop the button widgets made in step b for however many buttons you want.

  4. Functionality. Make a new Player Controller for the front end (not strictly necessary). See Common UI YouTube video at 1:16:11

## Checklist
- Create level for title screen
- Set level as default starting location
- Create UI widget
- Set loading screen or widget
- Disable movement
- Have widget capture input
- Set levels to load based on input
- Set sound effects